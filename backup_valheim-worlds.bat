@echo off
setlocal EnableDelayedExpansion

:: Config variables
:: Change these values to your need
set world_name=valhaller
set output_dir=%userprofile%\Desktop
set sevenzip="C:\Program Files\7-Zip\7z.exe"

:: Declare variables
set worlds_path=%userprofile%\AppData\LocalLow\IronGate\Valheim\worlds
set timestamp=%date:/=-%_%time::=%
set timestamp=%timestamp:.=%
set timestamp=%timestamp:,=%
set timestamp=%timestamp: =%

:: Check for Valheim worlds directory/files and 7zip
if not exist %worlds_path% (
  call :error_message "%worlds_path% not found!"
  exit /b 1
) else (
  if not exist %worlds_path%\%world_name%.db (
    call :error_message "%worlds_path%\%world_name%.db not found!"
    exit /b 1
) else (
  if not exist %worlds_path%\%world_name%.fwl (
    call :error_message "%worlds_path%\%world_name%.fwl not found!"
    exit /b 1
) else (
    if not exist %sevenzip% (
    call :error_message ""%sevenzip%" not found!"
  exit /b 1
) else (
    goto :compress_world_files
))))

:: Compress files and save in output_dir
:compress_world_files
%sevenzip% a -t7z %output_dir%\%world_name%_%timestamp%.7z %worlds_path%\%world_name%.db %worlds_path%\%world_name%.fwl
if not exist %output_dir%\%world_name%_%timestamp%.7z (
call :error_message "No files were compressed!"
exit /b 1
) else (
echo [SUCCESS] "Compressed archive: %output_dir%\%world_name%_%timestamp%.7z"
pause
exit /b 0
)

:error_message
echo [ERROR] %1
pause