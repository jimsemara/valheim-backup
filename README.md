<!-- Improved compatibility of back to top link: See: https://github.com/othneildrew/Best-README-Template/pull/73 -->
<a name="readme-top"></a>

[![MIT License][license-shield]][license-url]


<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

A handy script to backup local Valheim world files with one click or even automatically using a scheduler.

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple example steps.

### Prerequisites

* 7-Zip  
The script utilizes 7-Zip for archiving and compressing the world files.  
You can download the latest version over here: [https://www.7-zip.de/](https://www.7-zip.de/) 

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/jimsemara/valheim-backup.git
   ```
2. Change config variables in the script to your need
   ```sh
   world_name=valhaller
   output_dir=%userprofile%\Desktop
   sevenzip="C:\Program Files\7-Zip\7z.exe"
   ```
  
<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- USAGE EXAMPLES -->
## Usage
Simply run the script with Windows CMD shell or PowerShell.  
You can use a scheduler to automate the use of the script.  


<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- ROADMAP -->
## Roadmap

- [ ] Write the script in Python
- [ ] Add a GUI
- [ ] Add an upload feature
  
<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- LICENSE -->
## License

Distributed under the MIT License. See [`LICENSE`](https://gitlab.com/jimsemara/valheim-backup/-/blob/main/LICENSE) for more information.

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- CONTACT -->
## Contact

Achim S. - jimsemara@gmail.com

Project Link: [https://gitlab.com/jimsemara/valheim-backup](https://gitlab.com/jimsemara/valheim-backup)

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[license-shield]: https://img.shields.io/gitlab/license/jimsemara/valheim-backup.svg?style=for-the-badge
[license-url]: https://gitlab.com/jimsemara/valheim-backup/-/blob/main/LICENSE
